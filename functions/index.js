const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.firestore();


exports.roleAuthority = functions
    .region('europe-west3')
    .firestore.document('/users/{userId}').onCreate((snapshot, context) => {
        const user = snapshot.data();
        const userId = context.params.userId;

        console.log('onCreate user', userId, JSON.stringify(user));
        const role = {
            type: "Guest",
            email: user.email,
            name: user.name,
            created: new Date()
        };

        const createUser = snapshot.ref.set(user, {merge: true});
        const createRole = db.collection('roles').doc(userId).set(role);
        return Promise.all([createUser, createRole]);
    });

async function getRole(userId) {
    const snapshot = await db.collection("roles").doc(userId).get();
    return snapshot.data();
}

async function getUser(userId) {
    const snapshot = await db.collection("users").doc(userId).get();
    return snapshot.data();
}

exports.locationAuthority = functions
    .region('europe-west3')
    .firestore.document('/locations/{userId}').onWrite(async (change, context) => {
        const userId = context.params.userId;

        if (!change.after.exists) {
            return db.collection('userPoints').doc(userId).delete();
        }
        const location = change.after.data();

        const role = await getRole(userId);
        if (role === undefined) {
            return null
        }

        if (role.type === "Guest") {
            return null
        }

        const user = await getUser(userId);

        if (user === undefined) {
            return null
        }

        const userPoint = {
            id: userId,
            location: location.location,
            lastUpdatedTime: location.lastUpdatedTime,
            userName: user.name,
            description: location.description || ""
        };

        return db.collection('userPoints').doc(userId).set(userPoint);
    });

exports.pointAuthority = functions
    .region('europe-west3')
    .firestore.document('/points/{userId}').onWrite(async (change, context) => {
        const userId = context.params.userId;

        if (!change.after.exists) {
            return db.collection('pickets').doc(userId).delete();
        }
        const point = change.after.data();

        const role = await getRole(userId);

        console.log('onUpdate point', userId, JSON.stringify(point));

        if (role === undefined) {
            return null
        }

        if (role.type === "Guest") {
            return null
        }

        const user = await getUser(userId);

        if (user === undefined) {
            return null
        }

        const picket = {
            id: userId,
            location: point.location,
            startTime: point.startTime,
            endTime: point.endTime,
            lastUpdatedTime: point.lastUpdatedTime,
            userName: user.name,
            address: point.address,
            description: point.description
        };

        return db.collection('pickets').doc(userId).set(picket);
    });
